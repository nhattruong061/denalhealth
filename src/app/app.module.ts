import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactHeaderComponent } from './components/contact-header/contact-header.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BannerComponent } from './components/banner/banner.component';
import { MoveTopComponent } from './components/move-top/move-top.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { ServicesComponent } from './components/services/services.component';
import { AboutBottomComponent } from './components/about-bottom/about-bottom.component';
import { AppointmentComponent } from './components/appointment/appointment.component';
import { CopyRightComponent } from './components/copy-right/copy-right.component';
import { BlogComponent } from './components/blog/blog.component';
import { AboutComponent } from './components/about/about.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { BlogPageComponent } from './pages/blog-page/blog-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { GalleryPageComponent } from './pages/gallery-page/gallery-page.component';
import { ServicePageComponent } from './pages/service-page/service-page.component';
import { SinglePageComponent } from './pages/single-page/single-page.component';
import { DoctorsComponent } from './components/doctors/doctors.component';
import { JoinComponent } from './components/join/join.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { ContactComponent } from './components/contact/contact.component';
import { AlertComponent } from './components/alert/alert.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { JwtInterceptor } from './services/helpers/jwt.interceptor';
import { ErrorInterceptor } from './services/helpers/error.interceptor';
import { fakeBackendProvider } from './services/helpers/fake-backend';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    BlogComponent,
    ContactHeaderComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent,
    MoveTopComponent,
    TestimonialsComponent,
    ServicesComponent,
    AboutBottomComponent,
    AppointmentComponent,
    CopyRightComponent,
    AboutPageComponent,
    BlogPageComponent,
    ContactPageComponent,
    GalleryPageComponent,
    ServicePageComponent,
    SinglePageComponent,
    DoctorsComponent,
    JoinComponent,
    GalleryComponent,
    ContactComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    LoginPageComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
