import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutBottomComponent } from './about-bottom.component';

describe('AboutBottomComponent', () => {
  let component: AboutBottomComponent;
  let fixture: ComponentFixture<AboutBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
